import kotlin.properties.Delegates

interface TextChangedListener {

    fun onTextChanged(newText: String)
}

class PrintingTextChangedListener : TextChangedListener {

    override fun onTextChanged( newText: String) =
            println("Texto cambiado a: $newText")
}

class ObservableObject(listener:TextChangedListener){
    var text: String by Delegates.observable(initialValue = "",
            onChange = {_,_,new -> listener.onTextChanged(new)})
}

fun main (){
    val observableObject = ObservableObject(PrintingTextChangedListener())
    observableObject.text="Aplicaciones"
    observableObject.text="Móviles"
}
main()