open class Computer : Cloneable {
    private var procesador: Int = 0
    private var moderboard: String? = null
    var model : String? = null
        private set

    init {
        moderboard = "ASUS B350"
        model = "HP pavilion"
        procesador = 4
    }

    public override fun clone(): Computer {
        return Computer()
    }

    fun makeAdvanced(){
        moderboard = "MSI B50"
        model = "DELL"
        procesador = 6
    }
}

fun makeMasterRacer (basicComputer: Computer): Computer {
    basicComputer.makeAdvanced()
    return basicComputer
}

fun main(args: Array<String>) {
    val computer = Computer()
    val basicComputer = computer.clone()
    val advanceComputer = makeMasterRacer(basicComputer)
    println("Patrón de diseño Prototipo: " + advanceComputer.model )
}


