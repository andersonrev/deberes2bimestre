fun main() {
    User.printName()
    User.name = "Peter"
    User.printName()
    User.printName()
    User.printName()
}

object User{
    var name = "Anderson"
    var lastname = "Revelo"

    init {
        println("Singleton invoked")
    }

    fun printName() = println(name + ' '+ lastname)
}

/*
Singleton invoked
Anderson Revelo
Peter Revelo
Peter Revelo
Peter Revelo

*/